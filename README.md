/***CSS Animation***/

## 1. Overview of CSS Transitions and Transforms:
### CSS transform basics
- Transforms change the shape and position of the affected content by modifying the coordinate space.
- Transforms do not disrupt the normal document flow.
- Some methods of transform:
    + translate(x, y): can take units such as: px, ems, percentages. Percentages refer to the object itself, not its container.
    + rotate(): rotates an element clockwise or counter-clockwise according to a given degree
        + Ex: rotate(20deg); rotate(-20deg)
    + scale(): increases or decreases the size of an element (according to the parameters given for the width and height).
        + Ex: scale(1, 2)
    + skewX(): skews an element along the X-axis by the given angle.
    + skewY(): skews an element along the Y-axis by the given angle.
    + matrix(scaleX(),skewY(),skewX(),scaleY(),translateX(),translateY())

### Simple 3D transforms
- perspective property: defines how many pixels a 3D element is placed from the view.
- The perspective property only affects 3D transformed elements!
- translate3d(x, y, z)
### CSS transitions
- To create a transition effect, I need to specify two things:
    + the CSS property you want to add an effect to
    + the duration of the effect
- `transition-timing-function`: property specifies the speed curve of the transition effect.
    + ease - slow start, then fast, then end slowly (this is default)
    + linear - the same speed from start to end
    + ease-in - slow start
    + ease-out - slow end
    + ease-in-out - slow start and end
    + cubic-bezier(n,n,n,n) - lets you define your own values in a cubic-bezier function
- `transition-delay`: specifies a delay (in seconds) for the transition effect.
- Can transition for several property values, separated by ","
- First number for duration, second number for delay

## 2. Understanding CSS Animations
### Animation basics
- 2 steps to a CSS Animation:
    + Define the animation
    + Assign it to a specific element or elements
- Creating CSS Animation by defining it's keyframes using @keyframe rule
    + Keyframes are a list describing what should happen over the course of the animation
    + Give animation a name.
- Add some properties:
    + animation-name:
    + animation-duration
    + animation-timing-function: specifies the speed curve of an animation.
    + animation-iteration-count: specifies the number of times an animation should be played.

### Use animation-delay and animation-fill-mode
- animation-delay:
- animation-fill-mode: specifies a style for the element when the animation is not playing (when it is finished, or when it has a delay).

### Use animation-direction
- animation-direction: defines whether an animation should play in reverse direction or in alternate cycles.
- animation-direction: normal|reverse|alternate|alternate-reverse|initial|inherit;
    + normal: all iterations of the animation are played as specified, keyframes play from start to end.
    + reverse: all iterations of the animation are played in the reverse direction, keyframes play end to start
    + alternate: animation direction is alternated with each iteration of the animation, keyframes play from start to end, then end to start and continue alternating
    + alternate-reverse: animation direction is alternated with each iteration of the animation, keyframes play from end to start, then start to end and continue alternating

### Timing functions and easing
- animation-timing-function Options: predefined keywords, cubic-bezier functions, steps.
    + I can custom speed of animation with `cubic-bezier`
    + Ref: http://cubic-bezier.com

## 3. CSS Animation Building Blocks
### Infinitely looping animation
### Animate an element into place
### Pause and play with animation-play-state
- animation-play-state: running|paused, default value is running
### Animate 3D transforms
- transform-origin: allows change the position of transformed elements. 2D transformations can change the x- and y-axis of an element. 3D transformations can also change the z-axis of an element.
### Prepare an image to use as a sprite
- Exporting the images series:
    + Export a series of .jpgs or .pngs
    + Select high quality for the image quality settings
    + Don't export progressive .jpgs
    + Get smoother animation with more images but also larger file size

### Animate sprite images with steps
- step(n): separate image into n equal parts
### Chain multiple animations
- We can chain multiple animations

## 4. Applying CSS Animations to SVG
### Prepare an SVG for animation
- Why SVG?
    + Resolution independent
    + small file size
    + Accessibility
- Use independent shapes
    + Avoid merging paths
    + Layer shapes instead of knocking them out of the background shapes
- Keep drawings simple
    + Saves file size
    + Means easier animation
- Use simple shapes instead of paths
    + When possible, draw with the shape tools insteads of making custom paths
- Live text or outlined text?
    + You need to decide early on if you want your text to be live or not
    + Live text is more accessible but may require web fonts to be loaded
    + Outlined text may result in a smaller file size.

### Export SVG from Illustrator
### Animate SVG with CSS
- Only som eSVG properties are exposed to CSS
- More will be exposed in SVG 2.0
- Most useful animatable properties:
    + SVG fill
    + Opacity
    + CSS transform on SVG elements
- Transform Origins:
    + HTML elements default to a transform origin at their middle point (50%, 50%)
    + SVG elements default to a transform origin at the top left of the SVG canvas.
- There are two JavaScript animation libraries that do a particularly good job at animating SVG.
    + GreenSock animation library, which has some plugins specific for SVG
    + Snap SVG, which as the name suggests, is built specifically just for animating SVG.

## 5. Performance, Browser support and Fallbacks
### When to use CSS animations
- When to use CSS keyframe animation
    + Demonstrations or linear animations
    + Very simple state-based animation
- When to use CSS transitions:
    + Toggling between two states
    + Animating on :hover, :active, :checked
- When to use JS:
    + Complex state animations
    + Dynamic animations
    + Physics
    + Support older browsers
- What about SVG?
    + Vector-based animation
    + Icons or other self-contained animations
- Web animation is a team effort:
    + Don't have to choose just one web animation technology only
    + Combine Js for the logic and CSS for the motion
    + Combine  JS, CSS with SVG
- Consider audience and content
    + How critical is the content?
    + Which browsers will your audience be using?
- When Animations are critical:
    + Critical content + a large percentage of less capable browsers?
    + Use JS for older browser Support
- When Animation is not critical:
    + Animated enhancements + a mix of browser capabilities?
    + Treat animation as progressive enhancement (CSS or JS or SVG)

### High-performance animations
- Most performant properties
    + Opacity
    + Scale, rotation and position with transform
    + 4 steps of the browser potentially has to go through to visually update any change on the page:
        + Style => Layout -> Paint -> composite
        + The fewer steps the browser has to go through to update a property, the more performant it will be
        + csstriggers.com
- The Translate 3D Hack:
    + Turns on hardware acceleration
    + Promotes objects to a new layer
    + Transform: translate3d(0, 0, 0);
- The will-change property
    + Notifies browsers that an object willl be animating for priority rendering
    + More fine-grained control than the translate3d "hack"

### Code organization and fallbacks
- Handling vendor prefixes:
    + Use an automated solution like Autoprefixer or similar
    + autoprefixer.github.io
- organizing CSS animation Code
    + Keep object specific animation code with the object's other styles
    + Centralize code that will be repurposed on multiple objects throughout a project.
- Fallbacks and Other Browsers
    + You can consider what your animated objects will look like if the animation never executes
    + Not playing may be an acceptable fallback state for nonsupporting browsers.
- Progressive enhancement
    + Think of animation as an enhancement for the browsers that support it
    + Make sure less capable devices or browsers still get a functional experience

## 6. Tools for creating CSS animations
### Helpful online tools for animations
- For custom easing ideas: `cubic-bezier.com` and `easings.net`
- To make your own complex animations, using matrix transforms have a look at `Bounce.js`.
- `SASS`: writing and organizing your CSS animation work.
- To get into some more advanced SVG animation, checking out two JavaScript animation libraries
- Browsing codepen.io is a great place to find animation inspiration and pick up some new coding ideas.
### Using browsers'animation inspection tools
